# Notebook

This notebook is accessible at the following URL <https://mko575.gitlab.io/notebook/>.

This is a "glorified" notebook containing howtos, tips and tricks related to MKO's development work. The content has not necessarily been tested, even less validated. There are no guarantee of correctness of the content of this repository.
