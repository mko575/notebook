+++
title = "Hugo"
menuPre = "<i class='fab fa-hugo'></i> "
weight = 10
+++

## Documentation

- https://gohugo.io/getting-started/quick-start/

## Initialization of a new website

Using [Git](/git), [Gradle](/gradle), and [Francois Staudt's Hugo Gradle plugin](https://plugins.gradle.org/plugin/io.github.fstaudt.hugo), it is easy to version the whole website sources (thus ensuring to _always_ be able to regenerate the website).

1. initialize a Git versioned project

```bash
git init
...
```

2. initialize a Gradle project

```bash
gradle ...
...
```

3. add [fstaudt.hugo](https://github.com/fstaudt/gradle-hugo-plugin) plugin
4. initialize a new Hugo website

`hugo new site src/hugo/...`

5. add a theme

```bash
git submodule add <theme\'s git repo> themes/<theme\'s name>
echo "theme = '<theme\'s name>'" >> hugo.toml
```
