+++
title = "Themes"
date = 2023-04-26T18:54:52+02:00
+++

## List of potentially interesting Hugo themes

The majority of Hugo themes are listed here: https://themes.gohugo.io/ (also at https://jamstackthemes.dev/#ssg=hugo).

### Potential documentation themes

- https://themes.gohugo.io/tags/docs/
- https://themes.gohugo.io/themes/ace-documentation/
- https://themes.gohugo.io/themes/hugo-book/
  - Menu hierarchy does not seem to work.
- https://themes.gohugo.io/themes/shadocs/
- https://themes.gohugo.io/themes/hugo-theme-next/
- https://themes.gohugo.io/themes/hugo-theme-techdoc/
- https://themes.gohugo.io/themes/hugo-whisper-theme/
- https://mcshelby.github.io/hugo-theme-relearn/index.html

Experience report: https://eventuallycoding.com/2020/04/19/une-documentation-qui-vit

#### Relearn

Websites:
- https://mcshelby.github.io/hugo-theme-relearn/
- https://github.com/McShelby/hugo-theme-relearn
- https://themes.gohugo.io/themes/hugo-theme-relearn/

#### Hugo Book

Websites:
- https://github.com/alex-shpak/hugo-book
- https://themes.gohugo.io/themes/hugo-book/
- demos:
  - https://hugo-book-demo.netlify.app/
  - https://jamstackthemes.dev/demo/theme/hugo-book/

#### Docsy

[Docsy](https://www.docsy.dev/) seems to be a powerful Hugo theme for documentations. However, it seems a bit heavy.

#### Orchid

[Orchid](https://github.com/orchidhq/Orchid) would have been a great solution for Java projects documentation. It has a dedicated [Javadoc plugin](https://orchid.run/plugins/orchidjavadoc). However, it does not seem to be maintained anymore.

See https://s01.oss.sonatype.org/content/repositories/snapshots/io/github/copper-leaf/orchid/orchid-core/ for the latest version?

## Documenting Java Projects

With regard to Javadoc documentation, see:
* https://github.com/dburkart/javadoc2md
* https://github.com/paul-hammant/qdox
* https://xvik.github.io/gradle-mkdocs-plugin/2.0.1/getting-started/