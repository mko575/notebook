+++
title = "Front Matter"
weight = 210
+++

[Front Matter](https://frontmatter.codes/) is a headless CMS that can integrate into [Visual Studio Code (VSCode)](https://code.visualstudio.com/). It helps writing content.

Documentation is available here: https://frontmatter.codes/docs.

GitHub repository: https://github.com/estruyf/vscode-front-matter.