+++
title = "Headless CMS"
weight = 310
+++

It is possible to simplify the creation of content using _headless CMS_ (Content Management Systems).

## Documentation

* https://www.netlify.com/blog/complete-guide-to-headless-cms/#popular-headless-cms-platforms-with-free-starter-templates
* https://www.netlify.com/blog/2020/08/11/deploy-a-strapi-and-react-blog-on-netlify/
* https://github.com/TryGhost/eleventy-starter-ghost/blob/main/README.md
* https://github.com/forestryio/novela-hugo-starter/blob/master/README.md (outdated? Forestry is now Tina.)
* https://jamstack.org/headless-cms/