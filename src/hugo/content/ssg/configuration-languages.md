+++
title = "Configuration languages"
menuTitle = "Config. lang."
weight = 110
+++

There exists many different languages oriented towards writing configuration files. Among those we can cite TOML, YAML, JSON, CSV, INI, ...

Regarding the difference between [YAML](https://yaml.org/) and [TOML](https://toml.io/en/) (appart the fact that in YAML indentation has a semantics, which is really strange to me; and that TOML does not provide a way to include other TOML files), the bottom line is that YAML is way more expressive and flexible than TOML. Which implies that writting and parsing YAML files is more difficult than for TOML files (including for providing good errors feedbacks). If you only want to define (ordered) sets of key/value pairs, you are probably better of with TOML. However, to define complexe (potentially cyclic) structures, YAML is probably a better (only) choice. A good answer to the question of there differences is available [here](https://stackoverflow.com/a/65289463).

## Links

- Specifications:
  - [TOML specifications](https://toml.io/en/v1.0.0)
  - [YAML specifications](https://yaml.org/spec/1.2.2/)
