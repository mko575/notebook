+++
archetype = "chapter"
title = "Static Site Generators (SSG)"
menuTitle = "SSG"
alwaysopen = false # Should the menu be expanded or not by default
ordersectionsby = "weight"
+++

Regarding Static Site Generators (SSG), I am more interested in "single binary" ones, whose generator can be versioned with the rest of the website sources. Such SSGs include: [Hugo](hugo/), implemented in [Go](https://go.dev); and [Zola](zola/), implemented in [Rust](https://www.rust-lang.org).

[JBake](https://jbake.org/) could also be a solution, however it does not seem to be maintained anymore.
