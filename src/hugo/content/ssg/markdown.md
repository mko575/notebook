+++
title = "Markdown"
weight = 120
+++

## Markdown Dialects

Various MarkDown dialects exists. By default, use [CommonMark](https://commonmark.org/help/). Hugo uses [Goldmark](https://github.com/yuin/goldmark/), a CommonMark-compliant MarkDown parser. It seems [GFM: GitHub Flavored Markdown]() may also works.


## Headers

 Headers in TOML are introduced and closed by `+++`

 ```md
 +++
 title = "This is my Title"
 +++
 ``` 

 Headers in YAML are introduced and closed by `---`

 ```md
 ---
 title: This is my Title
 ---
 ``` 