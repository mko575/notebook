+++
archetype = "chapter"
title = "SecDevOps: Secure Development, Delivery (and Operation)"
menuTitle = "SecDevOps"
menuPre = "<i class='far fa-file-shield'></i> "
alwaysopen = false # Should the menu be expanded or not by default
ordersectionsby = "weight"
+++

Here are a few links related to SecDevOps. They

## Frameworks

- [NIST Framework](https://www.nist.gov/cyberframework)
- [ISO 27001 Information Security Management (ISMS)](https://www.iso.org/isoiec-27001-information-security.html)
- [NIST SP 800-53](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-53r4.pdf)
- [CIS Controls](https://www.cisecurity.org/controls/)
- [ISO 27005:2018 Information Security Risk Management](https://www.iso27001security.com/html/27005.html)
- [FAIR risk management](https://www.fairinstitute.org/fair-book)
- [PCI DSS](https://en.wikipedia.org/wiki/Payment_Card_Industry_Data_Security_Standard)
- [COBIT](https://en.wikipedia.org/wiki/COBIT)
- [HIPAA](https://en.wikipedia.org/wiki/Health_Insurance_Portability_and_Accountability_Act)
- [NIST CSF](https://nvlpubs.nist.gov/nistpubs/CSWP/NIST.CSWP.04162018.pdf)

### OpenSSF: Open Source Security Foundation

- [OpenSSF Best Practices Badge Program](https://www.bestpractices.dev)
- [OpenSSF Scorecard](https://securityscorecards.dev/)
  - [GitHub repo](https://github.com/ossf/scorecard)
  - [GitHub Action for Scorecard](https://github.com/ossf/scorecard-action)
- [Concise Guide for Developing More Secure Software](https://best.openssf.org/Concise-Guide-for-Developing-More-Secure-Software.html)
- [Concise Guide for Evaluating Open Source Software](https://best.openssf.org/Concise-Guide-for-Evaluating-Open-Source-Software)

#### Blog posts

- [Unlock the Keys to Improved Software Security](https://openssf.org/blog/2024/05/13/unlock-the-keys-to-improved-software-security/)
- [Where Does Your Software (Really) Come From?](https://openssf.org/blog/2024/05/17/where-does-your-software-really-come-from/)
- [Mitigating Attack Vectors in GitHub Workflows](https://openssf.org/blog/2024/08/12/mitigating-attack-vectors-in-github-workflows/)

### Linux Foundation Security

- https://www.linuxfoundation.org/lf-security


## Tools

- [OWASP dep-scan](https://github.com/owasp-dep-scan/dep-scan): next-generation security and risk audit tool based on known vulnerabilities, advisories, and license limitations for project dependencies. Both local repositories and container images are supported as the input, and the tool is ideal for integration.

- [SigStore](https://www.sigstore.dev/) is an open source project for improving software supply chain security. The Sigstore framework and tooling empowers software developers and consumers to securely sign and verify software artifacts such as release files, container images, binaries, software bills of materials (SBOMs), and more. Signatures are generated with ephemeral signing keys so there’s no need to manage keys. Signing events are recorded in a tamper-resistant public log so software developers can audit signing events.

- [LFX Security](https://lfx.linuxfoundation.org/tools/security/) provides a clear view into security for project stakeholders through automated scans and fix recommendations, enabling developers to identify and resolve vulnerabilities quickly.

- [OpenChain](https://www.openchainproject.org/) provides security assurance programs, process management standards, reference material, a focused community and international partners to build a trusted software supply chain. It is the home of ISO/IEC 5230 and ISO/IEC 18974.

- [Confidential Computing](https://confidentialcomputing.io/): Protecting data in use by performing computation in a hardware-based, attested Trusted Execution Environment, increasing the security assurances for organizations that manage sensitive and regulated data.

- [System Package Data Exchange (SPDX®)](https://spdx.dev/): A freely available international open standard for Software Bill of Materials (SBOMs), communicating release information such as name, version, components, licenses, copyrights, superset profiles, and security references.

- [Trusty](https://www.trustypkg.dev/), a free-to-use web app that provides data and analysis for developers on the supply chain risk of their open source dependencies. Our risk model incorporates Sigstore signatures and metadata to gauge the trustworthiness of open source packages along with other project signals and community metrics.

- [Minder](https://stacklok.com/minder), a software supply chain security platform that is available as a self-hosted platform or as-a-service (free for public repos). Minder helps maintainers more easily use open source projects like OpenSSF’s Sigstore, SLSA and OSV schema to secure their project repos, software artifacts, and build pipelines. Minder provides an out-of-the-box policy template enabling users to verify that container images in developers’ build pipelines have been signed using Sigstore. When the signing certificate includes build metadata, Minder can verify that artifacts originate from a specific source code repository and CI workflow. Minder can also enforce policy on provenance metadata by verifying SLSA attestations. It integrates with GitHub’s new Artifact Attestations feature to discover Sigstore-signed supply chain data for any artifact.

- [OpenVEX](https://openssf.org/projects/openvex/) and [Protobom](https://openssf.org/projects/protobom/), two OpenSSF projects focused on SBOM standardization and adoption. 

- [StepSecurity Secure-Repo](https://github.com/step-security/secure-repo): Automatically apply security best practices in your GitHub repository

- [Dependabot for GitLab](https://dependabot-gitlab.gitlab.io/dependabot/): Orchestrator for dependabot-core library to create dependency update merge requests for GitLab projects

- [Renovate Bot](https://docs.renovatebot.com/): Automated dependency updates. Multi-platform and multi-language.

## Courses, Training and Certifications

- [LF Training and Certification](https://training.linuxfoundation.org/cybersecurity/) Learners from around the world gain marketable open source skills as well as sought-after, verifiable certifications, including in the important area of security.
