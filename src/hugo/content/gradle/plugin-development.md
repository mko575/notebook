+++
title = "Gradle Plugin Development"
description = "This page contains information related to the development of Gradle Plugins."
+++

## Kotlin DSL

* Kotlin DSL Primer: https://docs.gradle.org/current/userguide/kotlin_dsl.html
* Kotlin DSL Reference for Gradle: https://docs.gradle.org/current/kotlin-dsl/index.html

* Kotlin documentation: https://kotlinlang.org/docs/home.html

## Plugin DSL (Extension)

* [Gradle plugins and extensions: A primer for the bemused](https://dev.to/autonomousapps/gradle-plugins-and-extensions-a-primer-for-the-bemused-51lp) (08/2021)
* [Understanding Gradle plugins: the provider API](https://melix.github.io/blog/2022/01/understanding-provider-api.html) (01/2022)

* Short answer for nested container-based extensions: https://stackoverflow.com/a/76163344

### Unsorted links

* https://docs.gradle.org/current/userguide/custom_gradle_types.html#collection_types
* https://blog.mrhaki.com/2016/02/gradle-goodness-using-nested-domain.html
* https://stackoverflow.com/questions/64717362/nested-extensions-in-gradle-plugin
* https://stackoverflow.com/questions/28999106/define-nested-extension-containers-in-gradle
* 