+++
title = "Gradle"
+++

[Gradle](https://gradle.org) is a [JVM](https://en.wikipedia.org/wiki/Java_virtual_machine)-based build system. Build scripts are written in [Groovy](https://groovy-lang.org) or [Kotlin](https://kotlinlang.org) and a mix of _plugin-specific_ configuration DSLs. Its documentation for the latest release is available [online](https//docs.gradle.org/current/userguide/userguide.html).

## Documentation

Gradle documentation includes: a [user manual](https://docs.gradle.org/current/userguide/userguide.html); a [DSL guide](https://docs.gradle.org/current/dsl); and [javadocs](https://docs.gradle.org/current/javadoc);

Samples of Gradle configuration for various types of projects can be found [here](https://docs.gradle.org/current/samples/index.html).

## Day to day management

### Upgrading the wrapper

Upgrading the wrapper is done with the command `./gradlew wrapper --gradle-version latest`. Documentation is avaliable at https://docs.gradle.org/current/userguide/gradle_wrapper.html#sec:upgrading_wrapper.

## Extending Gradle's "default" features

### Developing Gradle Tasks

Documentation: https://docs.gradle.org/current/userguide/more_about_tasks.html

### Developing Gradle Plugins

Documentation: https://docs.gradle.org/current/userguide/custom_plugins.html
