+++
archetype = "chapter"
title = "Fedora"
description = "This chapter contains various links and tips related to Fedora."
menuPre = "<i class='fab fa-fedora'></i> "
+++

## Upgrading Fedora

Official documentation:
* [General release upgrade information](https://docs.fedoraproject.org/en-US/quick-docs/upgrading-fedora-new-release/)
* [Upgrade using the __dnf__ program on the CLI](https://docs.fedoraproject.org/en-US/quick-docs/upgrading-fedora-offline/)
  * [Documentation in French](https://doc.fedora-fr.org/wiki/Mise_%C3%A0_niveau_de_Fedora#Avec_DNF)

It boils down to running the following commands (as administrator or with `sudo`):
```bash
dnf install dnf-plugin-system-upgrade
dnf upgrade --refresh
dnf clean all
```
reboot
```bash
dnf system-upgrade download --releasever=XX [--allowerasing] [--downloaddir DESTDIR]
dnf system-upgrade reboot
```

If some problems occur, the following commands may help:
: `dnf system-upgrade download --releasever=XX --allowerasing`

If `/` does ot contain enough space, use the `--downloaddir DESTDIR` to specify a place where to save package downloads. Avoid RAMfs directories.
See:
* https://discussion.fedoraproject.org/t/how-can-i-do-a-system-upgrade-when-im-low-on-disk-space/77558
