+++
archetype = "chapter"
title = "Bash"
+++

## Manipulating arguments

### Dealings with all arguments at once

Use:
- `$@` to get all arguments as separated strings
- `$*` to get all arguments as a single string
Further information is available in the following [documentation](https://www.gnu.org/software/bash/manual/bash.html#Special-Parameters) and [blog post](https://linuxopsys.com/topics/pass-all-arguments-in-bash).