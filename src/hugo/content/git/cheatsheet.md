+++
title = "Cheatsheet"
description = "This page summarizes various common Git commands."
menuPre = "<i class='fas fa-scroll'></i> "
+++

## Information

To get (fetch) all information about all branches from the remote:
: `git fetch --all`

<!-- -->

To show a "graphical" version of the logs in the terminal:
: `git log --graph --pretty=format:'\''%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'\'' --abbrev-commit`
: to be put in an alias {{% tt %}}alias git-log='...'{{% /tt %}} in {{% tt %}}.bashrc{{% /tt %}}.

## Updating

To (fetch and) update the current local branch from the remote:
: `git pull`

## Branching

To list all existing branches:
: `git branch -a`

<!-- -->

To switch to other branch:
: `git switch <branch-name>`
: `git checkout <branch-name>`

<!-- -->

To create a new branch:
: `git switch -c <branch-name>`
: `git checkout -b <branch-name>`

<!-- -->

To track it on the remote 'origin' repository:
: `git push (-u|--set-upstream) origin <branch-name>`

<!-- -->

To delete a branch:
: `git branch (-d|--delete) <branch-name>`

## Commit

### Commit to another branch

If you need to commit your changes to another "compatible" branch:
: `git checkout <ancestor-branch>`
: commit what you want in that ancestor branch

<!-- -->

If you need to commit your changes to a new branch and clean up the current one:
: `git switch -C <new-name>`
: `git commit -am "..."` + adding new files before
: `git switch -C <previous-name>`

<!-- -->

If you need to commit your changes to another branch with more control:
: commit what you can in the current branch
: `git stash`
: `git checkout <other-branch>`
: `git stash pop`
: commit what you want in that other branch
{{% expand title="Further information ..." %}}
Further information is available in [a StackOverflow answer](https://stackoverflow.com/a/2945904), [Git's stach documentation](https://git-scm.com/docs/git-stash).

See `git stash list `, `git stash apply ...`, `git stash -u` for finer control.
{{% /expand %}}

## Diffing

To have a graphical view of the differences between the current branch and another one:
: `git difftool -t meld <other-branch>`

## Merging

To merge a branch into the current local branch:
: `git merge <branch-name>`

## Undoing things

To reset to a specific commit:
: `git reset <commit-id>`

To undo all modifications (deleting them):
: `git reset --hard <commit-id>`
: to undo local modifications `git reset --hard HEAD`

To revert a file to its last committed state:
: `git revert <file>`

To revert to a specific commit (and creating a new "forward" commit for this modification):
: `git revert <commit-id>`

## Modules

To a module:
: `git submodule add <remote-repo-URL> [<local-copy-path>]`

Configuration file for the module is found in {{% tt %}}.git/modules/<local-copy-path>/config{{% /tt %}}.

To lits all first level modules:
: `git submodule`

To recursively describe the status of all modules:
: ``git submodule --quiet foreach --recursive '
  echo -e "\n
    \e[4m\e[1;96m$displaypath\e[0m (
      HEAD:\e[1;35m`git name-rev --name-only HEAD`\e[0m
      ; branch:\e[2;36m`git branch --show-current`\e[0m
      ; ?:\e[3;33m`git rev-parse --abbrev-ref HEAD`\e[0m
    )
  " 
  && git status --untracked-files=no
'``
{{% expand title="Further options and information ..." %}}
Other options are:
: ``git submodule --quiet foreach --recursive 'echo $displaypath \(`git branch --show-current`\)'`` (may emphasize detached HEAD)
: or ``git submodule --quiet foreach --recursive 'echo $displaypath \(`git rev-parse --abbrev-ref HEAD`\)'``
: or ``git submodule --quiet foreach --recursive 'echo $displaypath \(`git rev-parse --symbolic-full-name HEAD`\)'``
: or ``git submodule --quiet foreach --recursive 'echo $displaypath \(`git symbolic-ref -q HEAD`\)'``

Further information about retrieving names is available [on Baeldung website](https://www.baeldung.com/git-current-branch-name) and in [a StackOverflow answer](https://stackoverflow.com/a/61808046).
{{% /expand %}}
