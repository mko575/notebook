+++
archetype = "chapter"
title = "GitLab"
menuPre = "<i class='fab fa-gitlab'></i> "
+++

This chapter documents GitLab specific features.