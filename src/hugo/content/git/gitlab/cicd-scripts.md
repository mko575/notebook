+++
title = "CI/CD Scripts"
+++

Documentation on the `.gitlab-ci.yml` file is available at https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html.

To validate GitLab CI/CD scripts for a particular project, go to `CI/CD > Pipelines >> CI lint` on the project's GitLab webpage. More information is available at https://docs.gitlab.com/ee/ci/lint.html#simulate-a-pipeline.

{{% notice style="warning" title="Overloading jobs"%}}
Overloading a job 'key' will overwrite the previous value **even for 'array' keys**! In particular, it is not possible to _add_ `rules` to a job. You then need to copy all previous rules and add your owns.

This is also valide for the top level `variables` key which must be defined only once!
{{% /notice %}}

## GitLab's CI/CD Templates

All available templates are located at https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/ci/templates.

### SAST

For an exemple on configuring SAST, have a look at https://gitlab.com/mko575/notebook/-/blob/main/.gitlab-ci.yml.

Documentation is avalable at https://docs.gitlab.com/ee/user/application_security/sast/.

## Variables

Documentation about CI/CD variables is available at https://docs.gitlab.com/ee/ci/variables/.

To add Job-local conditional variable, have a look at [rules:variables](https://docs.gitlab.com/ee/ci/yaml/#rulesvariables).

## Caching and Artifacts

Documentation on caching and artifacts is available at https://docs.gitlab.com/ee/ci/caching/.

## Specific Use Case Examples

### Gradle

GitLab's default template for builds based on Gradle is available at https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Gradle.gitlab-ci.yml.

## Testing locally

{{% notice style="info" title="Deprecation of `exec` command"%}}
Solutions based on `gitlab-runner exec` are deprecated, and will be unavailable starting with version 17.0 of GitLab ([see here](https://docs.gitlab.com/runner/commands/#gitlab-runner-exec-deprecated)).
{{% /notice %}}

Potential tools:
- https://pypi.org/project/gitlabci-local/
- https://github.com/firecow/gitlab-ci-local

Installation of GitLab Runner:
- https://docs.gitlab.com/runner/install/linux-repository.html

Related documentation and discussions:
- https://docs.gitlab.com/runner/commands/index.html#configuration-file
- https://docs.gitlab.com/runner/commands/#gitlab-runner-exec-deprecated
- https://blog.stephane-robert.info/post/gitlab-valider-ci-yml/
- https://forum.gitlab.com/t/how-to-test-an-entire-pipeline-locally-that-contains-jobs-with-dependencies/50636/5
- https://stackoverflow.com/questions/32933174/use-gitlab-ci-to-run-tests-locally
  - https://stackoverflow.com/a/65920577
  - https://stackoverflow.com/a/36358790
- https://stackoverflow.com/questions/63766919/build-step-in-pipeline-is-failing-with-connection-refused-error-while-running-gi (running GitLab server and runner locally in docker)
- https://www.lambdatest.com/blog/use-gitlab-ci-to-run-test-locally/ (towards the end)
- https://gitlab.com/qontainers/pipglr
- https://www.lambdatest.com/blog/use-gitlab-ci-to-run-test-locally/
- https://forum.gitlab.com/t/how-to-test-an-entire-pipeline-locally-that-contains-jobs-with-dependencies/50636/9

### Potential Solutions

The following command has been proposed as a solution on [StackOverflow](https://stackoverflow.com/questions/32933174/use-gitlab-ci-to-run-tests-locally):
```bash
docker run --entrypoint bash --rm -w $PWD -v $PWD:$PWD -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest -c 'git config --global --add safe.directory "*";gitlab-runner exec docker test'
```

The following command has been proposed as a solution on [StackOverflow](https://stackoverflow.com/questions/72533905/how-to-use-the-docker-gitlab-runner-to-exec-stages-locally):
```bash
docker run --rm \
       -v $(pwd):/workspace \ # mount directory to the container
       --workdir /workspace \ # set working directory
       -v /var/run/docker.sock:/var/run/docker.sock \ # enable docker
       --privileged \
       --entrypoint="gitlab-runner" \ # set entrypoint
       gitlab/gitlab-runner:latest exec docker MY_JOB
```

```bash
docker exec -it my-running-container-name gitlab-runner exec docker linting
```

 
-        https://blog.stephane-robert.info/post/gitlab-valider-ci-yml/

### Troubleshooting

- https://stackoverflow.com/questions/52696763/preparation-failed-cannot-connect-to-the-docker-daemon-at-unix-var-run-docke
- https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29378

#### Podman

- https://podman.io/
- https://www.ionos.fr/digitalguide/serveur/know-how/podman-vs-docker/
- https://opensource.com/article/23/3/podman-gitlab-runners (promising ...)
  - https://gitlab.com/qontainers/pipglr
- https://docs.gitlab.com/runner/executors/docker.html#use-podman-to-run-docker-commands
