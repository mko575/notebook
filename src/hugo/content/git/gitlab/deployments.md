+++
title = "Deployments"
+++

## GitLab Pages

Related documentations and discussions:
- https://docs.gitlab.com/ee/user/project/pages/
- https://about.gitlab.com/stages-devops-lifecycle/pages/
- https://docs.framasoft.org/fr/gitlab/gitlab-pages.html (in french)
- https://gitlab.com/pages (various starting examples)
- https://gitlab.com/gitlab-org/gitlab-pages/-/issues/668 (Future feature? Allow customizing the artifacts path "public" in GitLab Pages)

Example of CI/CD scripts for Hugo: https://gitlab.com/pages/hugo/-/blob/main/.gitlab-ci.yml

For faster websites, assets can be automatically compressed: https://docs.gitlab.com/ee/user/project/pages/introduction.html#serving-compressed-assets

Access to the website can be controled by following this [documentation](https://repository.prace-ri.eu/git/help/user/project/pages/pages_access_control.md). A similar page on the official GitLab website probably exists.

## Artifacts

General documentation for artifacts is available in [GitLab's documentation](https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html).

Here's an example of gitlab-ci.yml fragment[^indentationPb] declaring artifacts for job JOB:
[^indentationPb]: markdown entry does not respect indentation
```yml
JOB:
  script: ...
  artifacts:
    paths:
      - file.pdf
      - path/*xyz/
```

### Link to artifacts

There exists a [direct URL to artifacts](https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html#from-a-url). For the latest execution of job `<job-id>` in branch `<branch-id>` of project `<project-id>`:
- for the whole artifacts, use URL: `https://gitlab.com/api/v4/projects/<project-id>/jobs/artifacts/<branch-id>/download?job=<job-id>`
- for the specific file `<file-path>`, use the URL:
  - `https://gitlab.com/api/v4/projects/<project-id>/jobs/artifacts/<branch-id>/raw/<file-path>?job=<job-id>`
  - or `https://gitlab.com/<project-id>/-/jobs/artifacts/<branch-id>/raw/<file-path>?job=<job-id>`

## Publishing to GitLab Registries

### Summary

This summarizes the step to use GitLab's registries to publish and use libraries. It assumes that the registry used is freely readable.

#### Publishing

[^differentRegistryURL]: The URL is different if you use a group registry ([see here](https://docs.gitlab.com/ee/user/packages/maven_repository/index.html?tab=gradle#endpoint-urls))

1. Create a new project P to serve only as package registry;
2. [Create a 'deploy token'](https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html#create-a-deploy-token) for this project;
3. Save the 'deploy token' in `~/.gradle/gradle.properties` ([Gradle doc.](https://docs.gradle.org/current/userguide/build_environment.html#sec:gradle_configuration_properties)) under variable name `<deploy-token-var-name>` (for example, 'mkoRegistryDeployToken')
4. Add or extend a 'publishing' section in your build scripts[^differentRegistryURL] (most likely in file build.gradle). For example in Kotlin:
    ```kotlin
    publishing {
        publications {
            create<MavenPublication>("library") {
                groupId = "${rootProject.group}.${rootProject.name}".toLowerCase()
                artifactId = "${rootProject.name}-${project.name}".toLowerCase()
                version = rootProject.version

                from(components["java"])
            }
        }
        repositories {
            maven {
                url = uri("https://gitlab.com/api/v4/projects/<project_id>/packages/maven")
                credentials(HttpHeaderCredentials::class) {
                    name = "Deploy-Token"
                    value = findProperty("<deploy-token-var-name>") as String?
                }
                authentication {
                    create("header", HttpHeaderAuthentication::class)
                }
            }
        }
    }
    ```

{{% notice style="info" title="Project ID"%}}
Be careful that the `<project-id>` is not the project name but a (digit-based) number identifying the project. GitLab's error codes are totally unhelpful to spot such misconfigurations.

Among other places, the 'Project ID' can be found in the 'General Settings' page of the project.
{{% /notice %}}

#### Using

1. Add or extend a 'repositories' section in your build scripts[^differentRegistryURL] (most likely in file build.gradle). For example in Kotlin:
    ```kotlin
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/<project_id>/packages/maven")
            name = "GitLab"
        }
    }
    ```
2. Add a dependency:
   ```kotlin
    dependencies {
        implementation 'kotlin-library-tutorial:lib:1.0.0'
    }
   ```

### Documentation

Related documentations and discussions:
- https://docs.gitlab.com/ee/user/packages/package_registry/index.html
- https://docs.gitlab.com/ee/user/packages/maven_repository/index.html?tab=gradle
- https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html
- https://docs.gradle.org/current/userguide/publishing_setup.html
- https://levelup.gitconnected.com/publish-your-first-package-with-gradle-and-gitlab-1993e4405629 (quite complete how-to with Gradle)
