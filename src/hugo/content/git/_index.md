+++
archetype = "chapter"
title = "Git"
description = "This chapter provide documentation related to Git."
menuPre = "<i class='fab fa-git-alt'></i> "
date = "2023-05-04T17:26:02.794Z"
+++

## Submodules

Documentation on submodules is available at:
- https://git-scm.com/book/en/v2/Git-Tools-Submodules
- https://git-scm.com/docs/git-submodule

## Interacting with Tags

Various documentation:
- https://devconnected.com/how-to-checkout-git-tags/
