+++
archetype = "home"
title = "MKO's Development Notebook"
date = 2023-04-26T18:54:52+02:00
+++

This is a "glorified" notebook containing how-tos, tips and tricks related to [MKO](https://gitlab.com/mko575/)'s development work. The content has not necessarily been tested, even less validated. They are no guarantee of correctness of the content of this repository.