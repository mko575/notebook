+++
archetype = "chapter"
title = "Java"
description = "This chapter contains information, tips and tricks about Java."
menuPre = "<i class='fab fa-java'></i> "
+++

## Repositories

###  Assured Open Source Software

Using Google's Assured Open Source Software (AOSS) repository may be a safer option. Information on how to use it is available [here](https://cloud.google.com/assured-open-source-software/docs/download-java-packages).