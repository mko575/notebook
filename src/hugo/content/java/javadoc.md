+++
title = "Javadoc"
description = "This page contains information related to Javadoc."
+++

## Documentation

Information on Javadoc is available at:
* [Oracle's Javadoc command documentation](https://docs.oracle.com/en/java/javase/17/docs/specs/man/javadoc.html)
* [Oracle's Help Center Javadoc Guide](https://docs.oracle.com/en/java/javase/17/javadoc/javadoc.html) for Java 17;
  * [Oracle's Help Center Javadoc Guide for Java 13](https://docs.oracle.com/en/java/javase/13/javadoc/javadoc.html);
* [the (old) Oracle's Javadoc mainpage](https://www.oracle.com/java/technologies/javase/javadoc-tool.html) which is a bit outdated;

## Using Javadoc

Javadoc for opens source projects hosted on [Maven Central Repository](https://central.sonatype.com/) can be found on [javadoc.io](https://www.javadoc.io/).

## Creating Javadoc documentation

### Tags

#### @value

 The [@value tag]() allows to inline the value of a final field inside the documentation.

### Generating the Javadoc

#### Command Line Options

-sourcepath
: specifies the list of directories where to look for packages.

-d directory
: specifies the destination directory where the javadoc tool saves the generated HTML files.

--javafx or -javafx
: enables JavaFX functionality.

-nonavbar
: prevents the generation of the navigation bar, header, and footer, that are usually found at the top and bottom of the generated pages. The -nonavbar option has no effect on the -bottom option. The -nonavbar option is useful when you are interested only in the content and have no need for navigation, such as when you are converting the files to PostScript or PDF for printing only.

-top html-code
: specifies the text to be placed at the top of each output file.

-Xdoclint
: enables recommended checks for problems in documentation comments.

### Integration with Gradle

Much information is available on the [Gradle's Javadoc task documentation](https://docs.gradle.org/current/dsl/org.gradle.api.tasks.javadoc.Javadoc.html).

A typical example of Javadoc task covering all sources would be:
```gradle
task myJavadocs(type: Javadoc) {
    source = sourceSets.main.allJava
}
```