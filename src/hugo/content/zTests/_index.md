+++
archetype = "chapter"
title = "Tests"
+++

This chapter does not contains docmentation per se. Its main purpose is to test various features I may want to use.

## Mermaid

[Mermaid](https://mermaid.js.org/intro/) allows to inline diagrams. Alternatives ar [PlantUML](https://plantuml.com/), [](), ...

```mermaid { align="center" zoom="true" }
graph LR;
    If --> Then
    Then --> Else
```

I do not really see why it does not work: probably not implemented in Relearn yet.

```mermaid { align="center" zoom="true" }
mindmap
Root(Racine)
    Content(Contenus)
        KB(Base de connaissances)
        Evt(Evènements)
    Users(Usagers)
        Providers(Fournisseurs)
        Consumers(Consomateurs)
```