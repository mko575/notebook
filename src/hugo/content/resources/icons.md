+++
title = "Icons"
description = "This page lists various Icon resources."
menuPre = "<i class='fas fa-icons'></i> "
+++

Free to use icons can be found on the following website:
* [Font Awsome Icon](https://fontawesome.com/v5/search?m=free)
* [Flaticon](https://www.flaticon.com/)
* [Academicons](https://www.flaticon.com/)

## Font Awsome Icons

[Font Awsome](https://fontawesome.com/) contains 2 main types of icons:
* [brand icons](https://fontawesome.com/v5/search?o=r&m=free&f=brands) in the {{% tt %}}fab{{% /tt %}} family;
* _regular_ icons, either in [solid ({{% tt %}}fas{{% /tt %}})](https://fontawesome.com/v5/search?o=r&m=free&s=solid) or [regular ({{% tt %}}far{{% /tt %}})](https://fontawesome.com/v5/search?o=r&m=free&s=regular) style.